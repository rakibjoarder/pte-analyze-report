import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc.dart';
export 'package:pte_score_analyzer/main_bloc/main_bloc.dart';

class MainBlocProvider extends InheritedWidget {
  final bloc = MainBloc();

  MainBlocProvider({Key key, Widget child}) : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  static MainBloc of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(MainBlocProvider)
              as MainBlocProvider)
          .bloc;
}
