import 'package:flutter/cupertino.dart';
import 'package:pte_score_analyzer/model/skills_model.dart';

class MainBloc extends BaseBloc {
  @override
  void dispose() {}
}

abstract class BaseBloc {
  PageController pageController;
  SkillModel model = new SkillModel();

  initBloc() {
    if (pageController == null) {
      pageController = PageController(initialPage: 0);
    }
  }

  void dispose() {
    pageController?.dispose();
  }
}
