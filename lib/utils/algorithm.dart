AnalyzAlgorithm(
    {writting,
    speaking,
    reading,
    listening,
    targetedscre,
    grammar1,
    writtenDiscourse1}) {
  var targetedScore = targetedscre;
  var w = writting; //writing
  var s = speaking; //speaking
  var r = reading; //reading
  var l = listening; //listening

  var grammar = grammar1;
  var writtenDiscourse = writtenDiscourse1;

  int pluscounter = 0;
  int minuscounter = 0;

  if (targetedScore >= 79) {
    w >= 70 ? pluscounter++ : null;
    s >= 70 ? pluscounter++ : null;
    r >= 70 ? pluscounter++ : null;
    l >= 70 ? pluscounter++ : null;
    w < 70 ? minuscounter++ : null;
    s < 70 ? minuscounter++ : null;
    r < 70 ? minuscounter++ : null;
    l < 70 ? minuscounter++ : null;

    if (pluscounter == 2 && minuscounter == 2) {
      print(
          'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation');
      return 'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation';
    }

    if (minuscounter == 4) {
      print(
          'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation');
      return 'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation';
    }
    if (w >= 79 && s < 79 && r < 79 && l < 79) {
      print('Pronunciation, Read aloud, Repeat sentence, Retell lecture');
      return 'Pronunciation, Read aloud, Repeat sentence, Retell lecture';
    }
    if (r < 79 && w >= 79 && l >= 79 && s >= 79) {
      print('Fill in the blanks Reading');
      return 'Fill in the blanks Reading';
    }

    if (r >= 79 && s >= 79 && l < 79 && w < 79) {
      if (grammar < 70) {
        print('Summarize spoken text, Write from dictation');
        return 'Summarize spoken text, Write from dictation';
      } else {
        print('Write from dictation, Listening fill in the blanks');
        return 'Write from dictation, Listening fill in the blanks';
      }
    }

    if (r >= 79 && w >= 79 && l >= 79 && s < 79) {
      print('Describe Image');
      return 'Describe Image';
    }
    if (r >= 79 && w >= 79 && l < 79 && s >= 79) {
      print('Repeat Sentence, Retell Lecture');
      return 'Repeat Sentence, Retell Lecture';
    }

    if (r >= 79 && w < 79 && l >= 79 && s >= 79) {
      if (writtenDiscourse < 65) {
        print('Essay');
        return 'Essay';
      } else {
        print(
            'spelling for Listening fill in the blanks and Write from dictation');
        return 'spelling for Listening fill in the blanks and Write from dictation';
      }
    }

    if (r < 79 && w < 79 && l >= 79 && s >= 79) {
      print('Fill in the Blanks Reading and Writing, Summarize written text.');
      return 'Fill in the Blanks Reading and Writing, Summarize written text.';
    }
  } else if (targetedScore >= 65) {
    minuscounter = 0;
    w < 65 ? minuscounter++ : null;
    s < 65 ? minuscounter++ : null;
    r < 65 ? minuscounter++ : null;
    l < 65 ? minuscounter++ : null;
    if (minuscounter >= 1) {
      print(
          'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation');
      return 'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation';
    }
  } else if (targetedScore <= 65) {
    minuscounter = 0;
    w < 50 ? minuscounter++ : null;
    s < 50 ? minuscounter++ : null;
    r < 50 ? minuscounter++ : null;
    l < 50 ? minuscounter++ : null;
    if (minuscounter >= 1) {
      print(
          'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation');

      return 'Read aloud, Repeat sentence, Retell lecture, Summarize Written Text, Fill in the blanks reading, Fill in the blanks reading and writing, Summarize spoken text, Fill in the blanks listening, Write from dictation';
    }
  }
}
