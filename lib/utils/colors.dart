import 'package:flutter/material.dart';

Color customizeWhiteColorForDarkmode(context){
      if(Theme.of(context).brightness == Brightness.light){
         return  Theme.of(context).primaryColor;
      }else{
         return Color.fromRGBO(255, 255, 255, 1);
      }
}

Color customizeWhiteColorForDarkmodeorBlack(context){
  if(Theme.of(context).brightness == Brightness.light){
    return  Colors.black87;
  }else{
    return Color.fromRGBO(255, 255, 255, 1);
  }
}


Color customizePinDotColorForDarkmode(context){
  if(Theme.of(context).brightness == Brightness.light){
    return  Theme.of(context).primaryColor;
  }else{
    return Color.fromRGBO(255, 255, 255, 1);
  }
}


Color customizePinScreenTextColorForDarkmode(context){
  if(Theme.of(context).brightness == Brightness.dark){
    return Colors.white;
  }else{
    return Colors.blueGrey;
  }
}


MaterialColor customColor(context,color){

  return  MaterialColor(
    color.value,
    <int, Color>{
      50: Color(color.value),
      100: Color(color.value),
      200: Color( color.value),
      300: Color( color.value),
      400: Color( color.value),
      500: Color( color.value),
      600: Color( color.value),
      700: Color( color.value),
      800: Color( color.value),
      900: Color( color.value),
    },
  );
}


