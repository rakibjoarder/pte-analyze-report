import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc_provider.dart';
import 'package:pte_score_analyzer/page/communicative_skill.dart';
import 'package:pte_score_analyzer/page/enabling_skill.dart';
import 'package:pte_score_analyzer/page/start_page.dart';
import 'package:pte_score_analyzer/page/targeted_score_page.dart';
import 'package:pte_score_analyzer/utils/screen_aware_size.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  MainBloc mainBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    mainBloc = MainBlocProvider.of(context);
    mainBloc.initBloc();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    mainBloc?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: AppBar(
        elevation: 0,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 200,
              width: double.infinity,
              color: Colors.red,
              child: Center(
                child: Hero(
                  tag: 'logo',
                  child: FlutterLogo(
                    size: screenAwareSize(120, context),
                    duration: Duration(milliseconds: 500),
                  ),
                ),
              ),
            ),
            Expanded(
              child: PageView(
                physics: NeverScrollableScrollPhysics(),
                controller: mainBloc.pageController,
                children: <Widget>[
                  StartPage(mainBloc: mainBloc),
                  CommunicativeSkill(
                    mainBloc: mainBloc,
                  ),
                  EnablingSkill(
                    mainBloc: mainBloc,
                  ),
                  TargetedScore(mainBloc: mainBloc)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
