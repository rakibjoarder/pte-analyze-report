class SkillModel {
  double listening;
  double reading;
  double speaking;
  double writing;
  double grammar;
  double oral;
  double pronunciation;
  double spelling;
  double vocabulary;
  double writtenDiscourse;
  double targetedScore;

  SkillModel(
      {this.listening,
      this.reading,
      this.speaking,
      this.writing,
      this.grammar,
      this.oral,
      this.pronunciation,
      this.spelling,
      this.vocabulary,
      this.targetedScore,
      this.writtenDiscourse});

  SkillModel.fromJson(Map<String, dynamic> json) {
    listening = json['listening'];
    reading = json['reading'];
    speaking = json['speaking'];
    writing = json['writing'];
    grammar = json['grammar'];
    oral = json['oral'];
    pronunciation = json['pronunciation'];
    spelling = json['spelling'];
    vocabulary = json['vocabulary'];
    targetedScore = json['targetedScore'];
    writtenDiscourse = json['writtenDiscourse'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['listening'] = this.listening;
    data['reading'] = this.reading;
    data['speaking'] = this.speaking;
    data['writing'] = this.writing;
    data['grammar'] = this.grammar;
    data['oral'] = this.oral;
    data['pronunciation'] = this.pronunciation;
    data['spelling'] = this.spelling;
    data['vocabulary'] = this.vocabulary;
    data['targetedScore'] = this.targetedScore;
    data['writtenDiscourse'] = this.writtenDiscourse;

    return data;
  }
}
