import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/home_page.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc_provider.dart';
import 'package:pte_score_analyzer/utils/algorithm.dart';
import 'package:pte_score_analyzer/utils/screen_aware_size.dart';

class AnalyzeReport extends StatefulWidget {
  MainBloc mainBloc;
  AnalyzeReport({this.mainBloc});
  @override
  _AnalyzeReportState createState() => _AnalyzeReportState(mainBloc: mainBloc);
}

class _AnalyzeReportState extends State<AnalyzeReport> {
  MainBloc mainBloc;
  _AnalyzeReportState({this.mainBloc});
  var data;
  var data1;
  bool isDone = false;
  var analyzeResult = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    analyzeResult = AnalyzAlgorithm(
        listening: mainBloc.model.listening,
        reading: mainBloc.model.reading,
        speaking: mainBloc.model.speaking,
        writting: mainBloc.model.writing,
        targetedscre: mainBloc.model.targetedScore,
        writtenDiscourse1: mainBloc.model.writtenDiscourse,
        grammar1: mainBloc.model.grammar);
    data = [
      new OrdinalSales('Listening', mainBloc.model.listening),
      new OrdinalSales('Reading', mainBloc.model.reading),
      new OrdinalSales('Speaking', mainBloc.model.speaking),
      new OrdinalSales('Writing', mainBloc.model.writing),
    ];
    data1 = [
      new OrdinalSales('Grammar', mainBloc.model.grammar),
      new OrdinalSales('Oral Fluency', mainBloc.model.oral),
      new OrdinalSales('Pronunciation', mainBloc.model.pronunciation),
      new OrdinalSales('Spelling', mainBloc.model.spelling),
      new OrdinalSales('Vocabulary', mainBloc.model.vocabulary),
      new OrdinalSales('Writtern Discourse', mainBloc.model.writtenDiscourse),
    ];

    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        isDone = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // For horizontal bar charts, set the [vertical] flag to false.
    return Scaffold(
        appBar: AppBar(
          title: Text("Analyze Report"),
        ),
        body: new Container(
          padding: EdgeInsets.all(screenAwareSize(20, context)),
          child: isDone == true
              ? Column(
                  children: <Widget>[
                    SizedBox(
                      height: screenAwareSize(12, context),
                    ),
                    Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Container(
                                color: Colors.red,
                                width: double.infinity,
                                child: Center(
                                  child: Text('Communicative Skills',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontSize:
                                              screenAwareSize(15, context))),
                                )),
                            Expanded(
                                child: HorizontalBarLabelChart.withSampleData(
                                    data)),
                          ],
                        )),
                    SizedBox(
                      height: screenAwareSize(30, context),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        children: <Widget>[
                          Container(
                              color: Colors.red,
                              width: double.infinity,
                              child: Center(
                                child: Text('Enabling Skills',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700,
                                        fontSize:
                                            screenAwareSize(15, context))),
                              )),
                          Expanded(
                              child:
                                  HorizontalBarLabelChart.withSampleData(data1))
                        ],
                      ),
                    ),
                    SizedBox(
                      height: screenAwareSize(30, context),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            Container(
                                decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(
                                            screenAwareSize(10, context)),
                                        topRight: Radius.circular(
                                            screenAwareSize(10, context)))),
                                width: double.infinity,
                                child: Center(
                                  child: Text('Analyze Result',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w700,
                                          fontSize:
                                              screenAwareSize(15, context))),
                                )),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[300],
                                  borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(
                                          screenAwareSize(10, context)),
                                      bottomLeft: Radius.circular(
                                          screenAwareSize(10, context)))),
                              padding:
                                  EdgeInsets.all(screenAwareSize(8, context)),
                              child: RichText(
                                text: TextSpan(
                                    text: 'Area to focus: ',
                                    style: TextStyle(
                                        color: Colors.red,
                                        fontWeight: FontWeight.w700,
                                        fontSize: screenAwareSize(15, context)),
                                    children: [
                                      TextSpan(
                                          text: ' $analyzeResult',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize:
                                                  screenAwareSize(13, context)),
                                          children: []),
                                    ]),
                              ),
                            ),
                            RaisedButton(
                              color: Colors.red,
                              textColor: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8))),
                              child: Text('Analyze Again'),
                              onPressed: () {
                                Navigator.of(context)
                                    .pushReplacement(MaterialPageRoute(
                                        builder: (context) => MainBlocProvider(
                                              child: MyHomePage(),
                                            )));
                              },
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                )
              : Center(
                  child: FlareActor("assets/image/analysis.flr",
                      alignment: Alignment.center,
                      fit: BoxFit.contain,
                      animation: "analysis"),
                ),
        ));
  }

  /// Create one series with sample hard coded data.
}

/// Sample ordinal data type.
class OrdinalSales {
  String year;
  double sales;

  OrdinalSales(this.year, this.sales);
}

/// Sample ordinal data type.

class HorizontalBarLabelChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  List<OrdinalSales> data;

  HorizontalBarLabelChart(this.seriesList, {this.animate});

  /// Creates a [BarChart] with sample data and no transition.
  factory HorizontalBarLabelChart.withSampleData(data) {
    return new HorizontalBarLabelChart(
      _createSampleData(data),

      // Disable animations for image tests.
      animate: true,
    );
  }

  // [BarLabelDecorator] will automatically position the label
  // inside the bar if the label will fit. If the label will not fit and the
  // area outside of the bar is larger than the bar, it will draw outside of the
  // bar. Labels can always display inside or outside using [LabelPosition].
  //
  // Text style for inside / outside can be controlled independently by setting
  // [insideLabelStyleSpec] and [outsideLabelStyleSpec].
  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      vertical: false,
      // Set a bar label decorator.
      // Example configuring different styles for inside/outside:
      //       barRendererDecorator: new charts.BarLabelDecorator(
      //          insideLabelStyleSpec: new charts.TextStyleSpec(...),
      //          outsideLabelStyleSpec: new charts.TextStyleSpec(...)),
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
      // Hide domain axis.
      domainAxis:
          new charts.OrdinalAxisSpec(renderSpec: new charts.NoneRenderSpec()),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData(data) {
    return [
      new charts.Series<OrdinalSales, String>(
          id: 'Sales',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (OrdinalSales sales, _) => sales.year,
          measureFn: (OrdinalSales sales, _) => sales.sales,
          data: data,
          // Set a label accessor to control the text of the bar label.
          labelAccessorFn: (OrdinalSales sales, _) => '${sales.year}')
    ];
  }
}
