import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc.dart';
import 'package:pte_score_analyzer/utils/screen_aware_size.dart';

class CommunicativeSkill extends StatefulWidget {
  MainBloc mainBloc;
  CommunicativeSkill({this.mainBloc});
  @override
  _CommunicativeSkillState createState() =>
      _CommunicativeSkillState(mainBloc: mainBloc);
}

class _CommunicativeSkillState extends State<CommunicativeSkill> {
  MainBloc mainBloc;
  var formKey = new GlobalKey<FormState>();
  _CommunicativeSkillState({this.mainBloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(screenAwareSize(15, context)),
      child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Text(
                'Communicative Skills',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: screenAwareSize(18, context)),
              ),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              Text(
                'Right now, millions of people find their employment affected by the COVID-19.',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.black45,
                    fontSize: screenAwareSize(10, context)),
              ),
              SizedBox(
                height: screenAwareSize(10, context),
              ),
              customTextFormField(
                  context: context, label: 'Listening', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Reading', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Speaking', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Writing', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Text(
                  'Next',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: screenAwareSize(16, context),
                  ),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                color: Theme.of(context).primaryColor,
                onPressed: () async {
                  if (formKey.currentState.validate()) {
                    FocusScope.of(context).requestFocus(FocusNode());
                    formKey.currentState.save();
                    mainBloc.pageController.animateToPage(2,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

TextFormField customTextFormField(
    {BuildContext context,
    label,
    icon,
    textController = null,
    initialValue,
    MainBloc mainBloc}) {
  return TextFormField(
    initialValue: initialValue,
    controller: textController,
    keyboardType: TextInputType.number,
    decoration: new InputDecoration(
      border: new OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      contentPadding: EdgeInsets.all(screenAwareSize(13, context)),
      labelText: label,
//      prefixIcon: Icon(
//        icon,
//      ),
    ),
    onSaved: (value) {
      if (label == 'Listening') {
        mainBloc.model.listening = double.tryParse(value);
      } else if (label == 'Reading') {
        mainBloc.model.reading = double.tryParse(value);
      } else if (label == 'Speaking') {
        mainBloc.model.speaking = double.tryParse(value);
      } else if (label == 'Writing') {
        mainBloc.model.writing = double.tryParse(value);
      }
    },
    validator: (value) {
      if (value.isEmpty) {
        return '$label score can\'t be empty';
      } else if (double.tryParse(value) > 100) {
        return '$label score must be between 1 - 100';
      } else {
        return null;
      }
    },
  );
}
