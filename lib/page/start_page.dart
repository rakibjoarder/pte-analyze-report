import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc.dart';
import 'package:pte_score_analyzer/utils/screen_aware_size.dart';

class StartPage extends StatefulWidget {
  MainBloc mainBloc;
  StartPage({this.mainBloc});
  @override
  _StartPageState createState() => _StartPageState(mainBloc: mainBloc);
}

class _StartPageState extends State<StartPage> {
  MainBloc mainBloc;
  _StartPageState({this.mainBloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(screenAwareSize(15, context)),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(
              'Wellcome to PTE Analyze Report',
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w700,
                  fontSize: screenAwareSize(18, context)),
            ),
            SizedBox(
              height: screenAwareSize(12, context),
            ),
            Text(
              'Right now, millions of people find their employment affected by the COVID-19 Right now, millions of people find their employment affected by the COVID-19 Right now, millions of people find their employment affected by the COVID-19Right now, millions of people find their employment affected by the COVID-19 Right now, millions of people find their employment affected by the COVID-19.',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.black45,
                  fontSize: screenAwareSize(13, context)),
            ),
            SizedBox(
              height: screenAwareSize(12, context),
            ),
            RaisedButton(
              padding: EdgeInsets.symmetric(horizontal: 50),
              child: Text(
                'Start',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: screenAwareSize(16, context),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              color: Theme.of(context).primaryColor,
              onPressed: () async {
                mainBloc.pageController.animateToPage(1,
                    duration: Duration(milliseconds: 500),
                    curve: Curves.easeIn);
              },
            ),
          ],
        ),
      ),
    );
  }
}
