import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc_provider.dart';
import 'package:pte_score_analyzer/utils/screen_aware_size.dart';

class EnablingSkill extends StatefulWidget {
  MainBloc mainBloc;
  EnablingSkill({this.mainBloc});
  @override
  _EnablingSkillState createState() => _EnablingSkillState(mainBloc: mainBloc);
}

class _EnablingSkillState extends State<EnablingSkill> {
  MainBloc mainBloc;
  var formKey = new GlobalKey<FormState>();
  _EnablingSkillState({this.mainBloc});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(screenAwareSize(15, context)),
      child: SingleChildScrollView(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              Text(
                'Enabling Skills',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w700,
                    fontSize: screenAwareSize(18, context)),
              ),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              Text(
                'Right now, millions of people find their employment affected by the COVID-19.',
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.black45,
                    fontSize: screenAwareSize(10, context)),
              ),
              SizedBox(
                height: screenAwareSize(10, context),
              ),
              customTextFormField(
                  context: context, label: 'Grammar', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Oral Fluency', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Pronunciation', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Spelling', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context, label: 'Vocabulary', mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              customTextFormField(
                  context: context,
                  label: 'Written Discourse',
                  mainBloc: mainBloc),
              SizedBox(
                height: screenAwareSize(7, context),
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(horizontal: 50),
                child: Text(
                  'Next',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: screenAwareSize(16, context),
                  ),
                ),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                color: Theme.of(context).primaryColor,
                onPressed: () async {
                  if (formKey.currentState.validate()) {
                    FocusScope.of(context).requestFocus(FocusNode());
                    formKey.currentState.save();
                    mainBloc.pageController.animateToPage(3,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeIn);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

TextFormField customTextFormField(
    {BuildContext context,
    label,
    icon,
    textController = null,
    initialValue,
    MainBloc mainBloc}) {
  return TextFormField(
    initialValue: initialValue,
    controller: textController,
    keyboardType: TextInputType.number,
    decoration: new InputDecoration(
      border: new OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      contentPadding: EdgeInsets.all(screenAwareSize(13, context)),
      labelText: label,
//      prefixIcon: Icon(
//        icon,
//      ),
    ),
    onSaved: (value) {
      if (label == 'Grammar') {
        mainBloc.model.grammar = double.tryParse(value);
      } else if (label == 'Oral Fluency') {
        mainBloc.model.oral = double.tryParse(value);
      } else if (label == 'Pronunciation') {
        mainBloc.model.pronunciation = double.tryParse(value);
      } else if (label == 'Spelling') {
        mainBloc.model.spelling = double.tryParse(value);
      } else if (label == 'Vocabulary') {
        mainBloc.model.vocabulary = double.tryParse(value);
      } else if (label == 'Written Discourse') {
        mainBloc.model.writtenDiscourse = double.tryParse(value);
      }
    },
    validator: (value) {
      if (value.isEmpty) {
        return '$label score can\'t be empty';
      } else if (double.tryParse(value) > 100) {
        return '$label score must be between 1 - 100';
      } else {
        return null;
      }
    },
  );
}
