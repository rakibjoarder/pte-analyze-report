import 'package:flutter/material.dart';
import 'package:pte_score_analyzer/home_page.dart';
import 'package:pte_score_analyzer/main_bloc/main_bloc_provider.dart';
import 'package:pte_score_analyzer/utils/screen_aware_size.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(seconds: 1), () {
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) {
        return MainBlocProvider(
          child: MyHomePage(),
        );
      }));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: Container(
        child: Center(
          child: Hero(
            tag: 'logo',
            child: FlutterLogo(
              duration: Duration(milliseconds: 800),
              size: screenAwareSize(120, context),
            ),
          ),
        ),
      ),
    );
  }
}
